# Akademy App 2019

## Frameworks used

- Vue
- Vuetify
- Yarn
- Vue PWA
- Workbox

## For development

- Clone this project and:

```bash
yarn install
```

- To run the project locally:

```bash
yarn serve
```

## For Deploy

- Build deploy content

```bash
yarn build
```

- Get the content from the dist folder and deploy it to your server.

### Images Licenses

Milano Duomo: This file is licensed under the Creative Commons Attribution 2.0 Generic license.

### Project License

This project is under MIT license.
