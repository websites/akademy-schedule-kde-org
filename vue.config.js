module.exports = {
    pwa: {
        workboxPluginMode: 'GenerateSW',
        workboxOptions: {
            runtimeCaching: [{
                urlPattern: new RegExp('https://conf.kde.org/en/Akademy2019/public/schedule.json'),
                handler: 'staleWhileRevalidate'
            }, {
                urlPattern: new RegExp('https://akademy-schedule.kde.org/bof.json'),
                handler: 'staleWhileRevalidate'
            }],
            skipWaiting: true,
            clientsClaim: true,
        }
    },
    publicPath: ""
}
